Initiation à IONIC4 avec Angular et création d'une appli hybride
avec authentification, connexion à des api (rest).
L'appli dispose de plusieurs pages, une d'athentification, une page meteo connecter
à l'api openweather, une page de recherche d 'images relié à l'api rest pixabay.
Un système de géolocalisation et connexion à la caméra du telephone.

Avant clonage s'assurer d'avoir node et npm à jours. Lors de la création d'un projet 
la dernière ionic/angular et cordova seront mis à jour automatiquement.

Cloner le projet, tout les plugins et versions sont à jour, rien à installer de plus.
Lancer le serveur avec ionic serve, si vous voulez le résultat sur smartphone, 
installer dev-app sur le tel et lancez le, lancer ensuite le serveur via
ionic serve --devapp et profiter.
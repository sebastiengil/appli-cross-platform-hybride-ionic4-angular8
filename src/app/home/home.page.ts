import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public contact = {
    name: "GIL",
      email:"sebastien.gil@yahoo.com",
      tel:"0647295891",
      logo:"assets/images/logo1.jpg",
      location:"assets/images/Capture_logo.png"
  }

  constructor() {}

}
